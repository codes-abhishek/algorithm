export {}

const oddEven = (n: number): string => {
  if((n & 1) == 0) {
    return "even number";
  } else {
    return "odd number";
  }
}

console.log(oddEven(5));
console.log(oddEven(10));